# DistanceRegressor

#### Test of a euclidian radial distance based regression. The distance of the sample to all the data are calculated 
#### and an average is calculated weighted by the distance. The greater the distance the smallest the weight. The sum
#### of the weights must be 1, what is achieved by softmax function.
#### Another approach is fitting a linear regression using only the timed columns to correct the fact that a 
#### average based approach cannot achieve future interpolation as it has not seem any future data.